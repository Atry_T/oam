package json;

/**
 *
 * @author Arty
 */
import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.sql.SQLException;
import java.util.Date;
import static javax.persistence.TemporalType.TIMESTAMP;
import static javax.ws.rs.client.Entity.json;
import static oracle.jrockit.jfr.events.ContentTypeImpl.TIMESTAMP;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/task")
public class TaskController {

    String conStr = "jdbc:sqlite:c:/temp/nolite.db";

    @RequestMapping(value = "/new", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<TaskPacket> moduleStart(@RequestParam("task") String task, @RequestParam("metric") String metric, @RequestParam("from") String from,
            @RequestParam("to") String to, @RequestParam("calcStart") String calcStart)
            throws SQLException, ClassNotFoundException {
        HttpStatus res = HttpStatus.OK;
        TaskPacket tp = new TaskPacket();
        Connection con = null;
        int aInt;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(conStr);
            Statement sta = con.createStatement();

            sta.setQueryTimeout(30);  // set timeout to 30 sec.                  

            java.sql.Date calcStartTS = new java.sql.Date((long) Integer.parseInt(calcStart) * 1000);
            sta.executeUpdate(
                    "INSERT INTO TASKS (TASK,METRIC,FROMTS,TOTS,CALCSTARTTS) VALUES ('" + task + "','" + metric + "','" + from + "','" + to + "'," + calcStartTS + ")"
            );

            ResultSet resultset = sta.getGeneratedKeys();

            aInt = resultset.getInt("last_insert_rowid()");

            sta.close();
            con.close();

            tp.setStatus("ok");
            tp.setTaskID("" + aInt);
            resultset.close();
        } catch (SQLException e) {
            tp.setStatus("error");
            tp.setMessage(e.getMessage().toString());
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<TaskPacket>(tp, res);
    }

    @RequestMapping(value = "/{taskId}/discard", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<TaskPacket> moduleStop(@PathVariable String taskId) throws ClassNotFoundException {
        TaskPacket tp = new TaskPacket();
        HttpStatus res = HttpStatus.OK;
        Connection con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(conStr);
            Statement sta = con.createStatement();

            sta.setQueryTimeout(30);  // set timeout to 30 sec.                  

            sta.executeUpdate(
                    "delete from TASKS where rowid = " + taskId
            );
            tp.setStatus("ok");
        } catch (SQLException e) {
            tp.setStatus("error");
            tp.setMessage(e.getMessage().toString());
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<TaskPacket>(tp, res);
    }

    @RequestMapping(value = "/{taskId}/status", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<TaskPacket> moduleStatus(@PathVariable String taskId) throws ClassNotFoundException {
        TaskPacket tp = new TaskPacket();
        HttpStatus res = HttpStatus.OK;
        Connection con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(conStr);
            Statement sta = con.createStatement();

            sta.setQueryTimeout(30);  // set timeout to 30 sec.                  

            ResultSet rs = sta.executeQuery(
                    "select count(*) AS rowcount from TASKS where rowid = " + taskId
            );
            rs.next();
            int count = rs.getInt("rowcount");
            rs.close();
            if (count == 1) {
                rs = sta.executeQuery(
                        "select result AS rowcount from TASKS where rowid = " + taskId
                );
                rs.next();
                String r = rs.getString("rowcount");
                if (rs.wasNull()) {
                    tp.setStatus("waiting");
                } else {
                    tp.setStatus("done");
                }
            } else {
                tp.setStatus("none");
            }

        } catch (SQLException e) {
            tp.setStatus("error");
            tp.setMessage(e.getMessage().toString());
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<TaskPacket>(tp, res);
    }

    @RequestMapping(value = "/{taskId}/result", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<TaskPacket> moduleResult(@PathVariable String taskId) throws ClassNotFoundException {
        TaskPacket tp = new TaskPacket();
        HttpStatus res = HttpStatus.OK;
        Connection con = null;
        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection(conStr);
            Statement sta = con.createStatement();

            sta.setQueryTimeout(30);  // set timeout to 30 sec.                  

            ResultSet rs = sta.executeQuery(
                    "select result AS rowcount from TASKS where rowid = " + taskId
            );
            rs.next();
            String r = rs.getString("rowcount");

            tp.setStatus("ok");
            tp.setMessage(r);

        } catch (SQLException e) {
            tp.setStatus("error");
            tp.setMessage(e.getMessage().toString());
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return new ResponseEntity<TaskPacket>(tp, res);
    }

}
