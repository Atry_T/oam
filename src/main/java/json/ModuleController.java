package json;

/**
 *
 * @author Arty
 */
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ModuleController {

    static String status = "stopped";

    @RequestMapping(value = "/start", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ModulePacket> moduleStart() throws IOException, ClassNotFoundException {
        ModulePacket mp = new ModulePacket();
        HttpStatus res = HttpStatus.OK;
        //flower.setMessage("error0");

        String path = new File(".").getCanonicalPath();
        String url = "jdbc:sqlite:c:/temp/nolite.db";
        Class.forName("org.sqlite.JDBC");
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();

                Statement sta = conn.createStatement();

                sta.setQueryTimeout(30);  // set timeout to 30 sec.

                sta.executeUpdate(
                        "create table IF NOT EXISTS TASKS"
                        + "( TASK VARCHAR(40),"
                        + " METRIC VARCHAR(40),"
                        + " FROMTS VARCHAR(40),"
                        + " TOTS VARCHAR(40),"
                        + " CALCSTARTTS DATETIME,"
                        + " RESULT VARCHAR(200) )"
                );
                sta.close();
                conn.close();
                mp.setStatus("ok");
                status = "ok";
            }

        } catch (SQLException e) {
            status = "error";
            mp.setStatus("error");
            mp.setStatus(e.getMessage());
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        status = "ok";
        return new ResponseEntity<ModulePacket>(mp, res);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "text/html")
    public @ResponseBody
    String Hello() {

        return "Offline Analitic Module version: fOff<BR><table style=\"border-collapse: collapse; width: 744pt;\" border=\"1\" width=\"992\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col style=\"mso-width-source: userset; mso-width-alt: 19616; width: 460pt;\" width=\"613\" /> <col style=\"mso-width-source: userset; mso-width-alt: 12128; width: 284pt;\" width=\"379\" /> </colgroup>\n"
                + "<tbody>\n"
                + "<tr style=\"mso-height-source: userset; height: 28.5pt;\">\n"
                + "<td class=\"xl65\" style=\"height: 28.5pt; width: 460pt;\" width=\"613\" height=\"38\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/status&quot;}\">/status</td>\n"
                + "<td class=\"xl65\" style=\"border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok\\&quot;, \\&quot;tasks\\&quot;: [\\&quot;id\\u2026\\u201d,\\u201did\\u2026\\u201d] }&quot;}\">{\"status\": \"ok\", \"tasks\": [\"id…”,”id…”] }</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 39.0pt;\">\n"
                + "<td class=\"xl65\" style=\"height: 39.0pt; border-top: none; width: 460pt;\" width=\"613\" height=\"52\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/start&quot;}\">/start</td>\n"
                + "<td class=\"xl65\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok\\&quot;} \\n{\\&quot;status\\&quot;: \\u201cerror\\&quot;, \\&quot;message\\&quot;: \\&quot;error description\\&quot;}&quot;}\">{\"status\": \"ok\"} <br /> {\"status\": “error\", \"message\": \"error description\"}</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 42.75pt;\">\n"
                + "<td class=\"xl65\" style=\"height: 42.75pt; border-top: none; width: 460pt;\" width=\"613\" height=\"57\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/stop&quot;}\">/stop</td>\n"
                + "<td class=\"xl65\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok\\u201d}\\n{\\&quot;status\\&quot;: \\&quot;error\\&quot;, \\&quot;message\\&quot;: \\&quot;error description\\&quot;}&quot;}\">{\"status\": \"ok”}<br /> {\"status\": \"error\", \"message\": \"error description\"}</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 52.5pt;\">\n"
                + "<td class=\"xl65\" style=\"height: 52.5pt; border-top: none; width: 460pt;\" width=\"613\" height=\"70\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/addTask?task=&lt;task&gt;&amp;metric=&lt;metric&gt;&amp;from=&lt;dFrom&gt;&amp;to=&lt;dTo&gt;&amp;calcStart=&lt;dtStart&gt;&quot;}\">/addTask?task=&lt;task&gt;&amp;metric=&lt;metric&gt;&amp;from=&lt;dFrom&gt;&amp;to=&lt;dTo&gt;&amp;calcStart=&lt;dtStart&gt;</td>\n"
                + "<td class=\"xl65\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok\\&quot;, taskId: \\&quot;3h2487h34hu\\&quot;}\\n{\\&quot;status\\&quot;: \\&quot;error\\&quot;,\\&quot;message\\&quot;: \\&quot;error description\\&quot;}&quot;}\">{\"status\": \"ok\", taskId: \"3h2487h34hu\"}<br /> {\"status\": \"error\",\"message\": \"error description\"}</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 31.5pt;\">\n"
                + "<td class=\"xl66\" style=\"height: 31.5pt; border-top: none; width: 460pt; padding-bottom: 2px; padding-top: 2px;\" width=\"613\" height=\"42\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/discardTask?taskId=\\&quot;3h2487h34hu\\&quot;&quot;}\">/discardTask?taskId=\"3h2487h34hu\"</td>\n"
                + "<td class=\"xl66\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok\\&quot;} \\n{\\&quot;status\\&quot;: \\&quot;error\\&quot;, \\&quot;message\\&quot;: \\&quot;error description\\&quot;} &quot;}\">{\"status\": \"ok\"} <br /> {\"status\": \"error\", \"message\": \"error description\"}</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 23.25pt;\">\n"
                + "<td class=\"xl66\" style=\"height: 23.25pt; border-top: none; width: 460pt;\" width=\"613\" height=\"31\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/checkTask?taskId=\\&quot;3h2487h34hu\\&quot;&quot;}\">/checkTask?taskId=\"3h2487h34hu\"</td>\n"
                + "<td class=\"xl66\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;waiting/processing/ended\\&quot;} &quot;}\">{\"status\": \"waiting/processing/ended\"}</td>\n"
                + "</tr>\n"
                + "<tr style=\"mso-height-source: userset; height: 30.75pt;\">\n"
                + "<td class=\"xl66\" style=\"height: 30.75pt; border-top: none; width: 460pt;\" width=\"613\" height=\"41\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;/getResult?taskId=\\&quot;3h2487h34hu\\&quot;&quot;}\">/getResult?taskId=\"3h2487h34hu\"</td>\n"
                + "<td class=\"xl65\" style=\"border-top: none; border-left: none; width: 284pt;\" width=\"379\" data-sheets-value=\"{&quot;1&quot;:2,&quot;2&quot;:&quot;{\\&quot;status\\&quot;: \\&quot;ok \\&quot;, timestamp=\\&quot;489r489j894...\\&quot;} \\r\\n{\\&quot;status\\&quot;: \\&quot;error\\&quot;, \\&quot;message\\&quot;: \\&quot;error description\\&quot;}&quot;}\">{\"status\": \"ok \", timestamp=\"489r489j894...\"} <br /> {\"status\": \"error\", \"message\": \"error description\"}</td>\n"
                + "</tr>\n"
                + "</tbody>\n"
                + "</table>";
    }

    @RequestMapping(value = "/stop", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ModulePacket> moduleStop() {
        ModulePacket mp = new ModulePacket();
        //flower.setMessage("error0");
        mp.setStatus("ok");
        status = "stopped";
        return new ResponseEntity<ModulePacket>(mp, HttpStatus.OK);
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<ModulePacket> moduleStatus() {
        ModulePacket mp = new ModulePacket();
        //mp.setMessage("error");
        mp.setStatus(status);
        HttpStatus res = HttpStatus.OK;
        if (status == "stopped") {
            res = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<ModulePacket>(mp, res);
    }

}
