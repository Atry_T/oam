/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Arty
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ModulePacket {

    private String status;
    private String message;

    public ModulePacket() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String ms) {
        this.message = ms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String st) {
        this.status = st;
    }
}
